<?php
class Empleados_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function get_empleados($id = FALSE)
        {
                if ($id === FALSE)
                {
                        $query = $this->db->get('empleados');
                        return $query->result_array();
                }

                $query = $this->db->get_where('empleados', array('id' => $id));
                return $query->row_array();
        }

        public function add_empleado()
        {
            $data = array(
                'nombre' => $this->input->post('nombre'),
                'apellido' => $this->input->post('apellido'),
                'edad' => $this->input->post('edad'),
                'empresa_id' => $this->input->post('empresa_id'),
                'tipo' => $this->input->post('tipo'),
                'skills' => $this->input->post('skills'),
            );

            return $this->db->insert('empleados', $data);
        }

        public function delete_empleado($id)
        {
            $this->db->delete('empleados', array('id' => $id));
        }

        public function update_empleado($id)
        {
            $data = array(
                'nombre' => $this->input->post('nombre'),
            );

            $this->db->update('empleados', $data, "id = '$id'");
        }
}
