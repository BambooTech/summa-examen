<?php
class Empresas_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }

        public function get_empresas($id = FALSE)
        {
            if ($id === FALSE)
            {
                    $query = $this->db->get('empresas');
                    return $query->result_array();
            }

            $query = $this->db->get_where('empresas', array('id' => $id));
            return $query->row_array();
        }

        public function get_empresa_empleados($id)
        {
                $query = $this->db->get_where('empleados', array('empresa_id' => $id));
                return $query->result_array();
        }

        public function get_promedio($id)
        {
                $this->db->select_avg('edad');
                $query = $this->db->get_where('empleados', array('empresa_id' => $id));
                return $query->row_array();
        }

        public function add_empresa()
        {
            $data = array(
                'nombre' => $this->input->post('nombre'),
            );

            return $this->db->insert('empresas', $data);
        }

        public function delete_empresa($id)
        {
            $this->db->delete('empresas', array('id' => $id));
            $this->db->delete('empleados', array('empresa_id' => $id));
        }

        public function update_empresa($id)
        {
            $data = array(
                'nombre' => $this->input->post('nombre'),
            );

            $this->db->update('empresas', $data, "id = '$id'");
        }
}
