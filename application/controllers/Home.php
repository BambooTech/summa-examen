<?php
class Home extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('empresas_model');
                $this->load->model('empleados_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
                $data['empleados'] = $this->empleados_model->get_empleados();
                $data['empresas'] = $this->empresas_model->get_empresas();

                $data['title'] = 'Ejercicio Práctico para Summa Solutions';
                $data['section'] = 'home';

                $this->load->view('admin/home', $data);
        }
}
