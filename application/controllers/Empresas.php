<?php
class Empresas extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('empresas_model');
                $this->load->model('empleados_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
                $data['empresas'] = $this->empresas_model->get_empresas();

                $data['title'] = 'Listado de Empresas';
                $data['section'] = 'empresas';

                $this->load->view('admin/list', $data);
        }

        public function view($id)
        {
                $data['empresa'] = $this->empresas_model->get_empresas($id);
                $data['empleados'] = $this->empresas_model->get_empresa_empleados($id);
                $data['promedio'] = $this->empresas_model->get_promedio($id);

                $data['title'] = $data['empresa']['nombre'];
                $data['section'] = 'empresas';

                $this->load->view('admin/detail', $data);
        }

        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Agregar Empresa';
            $data['section'] = 'empresas';

            $this->form_validation->set_rules('nombre', 'Nombre', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('admin/create', $data);
            }
            else
            {
                $this->empresas_model->add_empresa();
                redirect('/empresas');
            }
        }

        public function update_info($id)
        {
            $data['empresa'] = $this->empresas_model->get_empresas($id);

            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Editar Empresa';
            $data['section'] = 'empresas';

            $this->form_validation->set_rules('nombre', 'Nombre', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('admin/edit', $data);
            }
            else
            {
                $this->empresas_model->update_empresa();
                redirect('/empresas');
            }
        }

        public function delete($id)
        {
            $this->empresas_model->delete_empresa($id);
            redirect('/empresas');
        }
}
