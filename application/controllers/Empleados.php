<?php
class Empleados extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('empresas_model');
                $this->load->model('empleados_model');
                $this->load->helper('url_helper');
        }

        public function index()
        {
                $data['empleados'] = $this->empleados_model->get_empleados();
                $data['empresas'] = $this->empresas_model->get_empresas();

                $data['title'] = 'Listado de Empleados';
                $data['section'] = 'empleados';

                $this->load->view('admin/list', $data);
        }

        public function view($id = NULL)
        {
                $data['empleado'] = $this->empleados_model->get_empleados($id);

                if (empty($data['empleado']))
                    {
                        show_404();
                    }

                    $data['title'] = $data['empleado']['nombre'];
                    $data['section'] = 'empleados';

                    $this->load->view('admin/detail', $data);
        }

        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['empresas'] = $this->empresas_model->get_empresas();
            $data['title'] = 'Agregar Empleado';
            $data['section'] = 'empleados';

            $this->form_validation->set_rules('nombre', 'Nombre', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('admin/create', $data);
            }
            else
            {
                $this->empleados_model->add_empleado();
                redirect('/empleados');
            }
        }

        public function update_info($id)
        {
            $data['empleado'] = $this->empleados_model->get_empleados($id);

            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Editar Empleado';
            $data['section'] = 'empleados';

            $this->form_validation->set_rules('nombre', 'Nombre', 'required');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('admin/edit', $data);
            }
            else
            {
                $this->empleados_model->update_empleado();
                redirect('/empleados');
            }
        }

        public function delete($id)
        {
            $this->empleados_model->delete_empleado($id);
            redirect('/empleados');
        }
}
