-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 22, 2016 at 02:50 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.6.14-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `suma_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `edad` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL,
  `skills` varchar(100) NOT NULL,
  `empresa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `apellido`, `edad`, `tipo`, `skills`, `empresa_id`) VALUES
(1, 'Juan', 'Perez', 25, 'Diseñador', 'Diseñador Web', 1),
(2, 'Maria', 'Posadas', 23, 'Diseñador', 'Diseñador Gráfico', 5),
(3, 'Elias', 'Turrin', 29, 'Programador', 'PHP', 1),
(4, 'Federico', 'Lopez', 27, 'Programador', '.NET', 2),
(5, 'Pedro', 'Sanchez', 26, 'Programador', 'Python', 5),
(6, 'Josefina', 'Lorens', 26, 'Programador', 'PHP', 3),
(7, 'Luciana', 'Mauceri', 26, 'Diseñador', 'Diseñador Gráfico', 4),
(8, 'Javier', 'Rubio', 31, 'Programador', 'Python', 2),
(9, 'Martin', 'San Juan', 33, 'Diseñador', 'Diseñador Gráfico', 4),
(10, 'Tobias', 'Flescher', 27, 'Diseñador', 'Diseñador Web', 1),
(11, 'Ignacio', 'Perla', 26, 'Programador', 'PHP', 3),
(12, 'Jeremias', 'Springfield', 29, 'Programador', 'Python', 2),
(13, 'Lucas', 'Posadas', 32, 'Diseñador', 'Diseñador Gráfico', 4),
(14, 'Valeria', 'Garin', 25, 'Programador', '.NET', 3),
(15, 'Joaquin', 'Pedraza', 31, 'Programador', 'PHP', 1),
(16, 'Maria Belen', 'Salas', 23, 'Diseñador', 'Diseñador Gráfico', 3),
(17, 'Fernando', 'Alva', 34, 'Programador', '.NET', 4);

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`id`, `nombre`) VALUES
(1, 'Empresa Demo'),
(2, 'Empresa Dos'),
(3, 'Tres'),
(4, 'Empresa 44'),
(5, '500 Empresa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
