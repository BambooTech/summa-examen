<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <?php include 'common/head.php';?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Navigation -->
        <?php include 'common/nav.php';?>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if ($section == 'empresas')
                        {
                    ?>
                            <h1><?= $title; ?></h1>
                            <p class="lead">
                                El promedio de edad de los empleados de esta empresa es de: <strong><?= round($promedio['edad']); ?></strong>
                            </p>
                            <p>
                                Empleados disponibles en la empresa:
                            </p>
                            <table id="empresas-table" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Edad</th>
                                        <th>Empresa</th>
                                        <th>Profesional</th>
                                        <th>Skills</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Edad</th>
                                        <th>Empresa</th>
                                        <th>Profesional</th>
                                        <th>Skills</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    foreach ($empleados as $empleado): ?>
                                    <tr>
                                        <td>
                                            <?= $empleado['id']; ?>
                                        </td>
                                        <td>
                                            <?= $empleado['nombre']; ?>
                                        </td>
                                        <td>
                                            <?= $empleado['apellido']; ?>
                                        </td>
                                        <td>
                                            <?= $empleado['edad']; ?>
                                        </td>
                                        <td>
                                            <?= $empleado['empresa_id']; ?>
                                        </td>
                                        <td>
                                            <?= $empleado['tipo']; ?>
                                        </td>
                                        <td>
                                            <?= $empleado['skills']; ?>
                                        </td>
                                        <td>
                                            <a href="<?= site_url('empleados/'.$empleado['id']); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                            <a href="<?= site_url('empleados/update_info/'.$empleado['id']); ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            <a href="<?= site_url('empleados/delete/'.$empleado['id']); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                    <?php
                        } elseif ($section == 'empleados') {
                    ?>
                            <h1><?= $empleado['nombre']; ?> <?= $empleado['apellido']; ?></h1>
                            <p>
                                <strong>Edad:</strong> <?= $empleado['edad']; ?>
                                <br>
                                <strong>Empresa:</strong> <?= $empleado['empresa_id']; ?>
                                <br>
                                <strong>Profesional:</strong> <?= $empleado['tipo']; ?>
                                <br>
                                <strong>Skills:</strong> <?= $empleado['skills']; ?>
                            </p>
                    <?
                        }
                    ?>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        <div class="container-fluid">
            <hr>

            <?php include 'common/footer.php';?>
        </div>

        <?php include 'common/footer-scripts.php';?>

    </body>
</html>
