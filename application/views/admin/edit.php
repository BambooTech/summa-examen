<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <?php include 'common/head.php';?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Navigation -->
        <?php include 'common/nav.php';?>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h1><?= $title; ?></h1>
                    <p class="lead">
                        Completar con los datos.
                    </p>
                    <?php
                        if ($section == 'empresas') {
                            echo form_open('empresas/create')
                    ?>
                                <div class="form-group">
                                    <label for="nombre">Nombre*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre" value="<?= $empresa['nombre'];?>">
                                    <input type="hidden" name="id" value="<?= $empresa['id'];?>">
                                </div>
                                <button type="submit" class="btn btn-default">Editar</button>
                            </form>
                    <?
                        } elseif (condition) {
                            echo form_open('empleados/create')
                    ?>
                                <div class="form-group">
                                    <label for="nombre">Nombre*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre">
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Apellido*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre">
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Edad*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre">
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Empresa*</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Profesional*</label>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Skills*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre">
                                </div>
                                <button type="submit" class="btn btn-default">Agregar</button>
                            </form>
                    <?
                        }
                    ?>

                </div>
                <div class="col-xs-12">
                    <p>
                        <?php echo validation_errors(); ?>
                    </p>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        <div class="container-fluid">
            <hr>

            <?php include 'common/footer.php';?>
        </div>

        <?php include 'common/footer-scripts.php';?>

    </body>
</html>
