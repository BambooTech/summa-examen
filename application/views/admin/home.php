<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <?php include 'common/head.php';?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Navigation -->
        <?php include 'common/nav.php';?>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <h1><?= $title; ?></h1>
                    <p class="lead">
                        Developer: Elias Turrin
                        Web: <a href="http://eliasturrin.com.ar/#portfolio">www.eliasturrin.com.ar</a>
                        <br>
                        LinkedIn: <a href="https://ar.linkedin.com/in/elias-turrin-36672927">ar.linkedin.com/in/elias-turrin</a>
                    </p>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        <div class="container-fluid">
            <hr>

            <?php include 'common/footer.php';?>
        </div>

        <?php include 'common/footer-scripts.php';?>

    </body>
</html>
