<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <?php include 'common/head.php';?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Navigation -->
        <?php include 'common/nav.php';?>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <h1><?= $title; ?></h1>
                    <p class="lead">
                        Completar con los datos.
                    </p>
                    <?php
                        if ($section == 'empresas') {
                            echo form_open('empresas/create')
                    ?>
                                <div class="form-group">
                                    <label for="nombre">Nombre*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre">
                                </div>
                                <button type="submit" class="btn btn-default">Agregar</button>
                            </form>
                    <?
                        } elseif ($section == 'empleados') {
                            echo form_open('empleados/create')
                    ?>
                                <div class="form-group">
                                    <label for="nombre">Nombre*</label>
                                    <input type="text" class="form-control" id="Nombre" name="nombre">
                                </div>
                                <div class="form-group">
                                    <label for="apellido">Apellido*</label>
                                    <input type="text" class="form-control" id="Apellido" name="apellido">
                                </div>
                                <div class="form-group">
                                    <label for="edad">Edad*</label>
                                    <input type="text" class="form-control" id="Edad" name="edad">
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Empresa*</label>
                                    <select class="form-control" name="empresa_id">
                                        <?php foreach ($empresas as $empresa): ?>
                                            <option value="<?php echo $empresa['id']; ?>"><?php echo $empresa['nombre']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nombre">Profesional*</label>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="tipo" value="Diseñador" checked id="designer-opt">
                                            Diseñador
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="tipo" value="Programador" id="dev-opt">
                                            Programador
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="designer">
                                    <label for="nombre">Skills*</label>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="skills" value="Diseñador Gráfico">
                                            Diseñador Gráfico
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="skills" value="Diseñador Web">
                                            Diseñador Web
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="developer">
                                    <label for="nombre">Skills*</label>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="skills" value="PHP">
                                            PHP
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="skills" value=".NET">
                                            .NET
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                        <input type="radio" name="skills" value="Python">
                                            Python
                                        </label>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-default">Agregar</button>
                            </form>
                    <?
                        }
                    ?>

                </div>
                <div class="col-xs-12">
                    <p>
                        <?php echo validation_errors(); ?>
                    </p>
                </div>
            </div>
            <!-- /.row -->

        </div>
        <!-- /.container -->
        <div class="container-fluid">
            <hr>

            <?php include 'common/footer.php';?>
        </div>

        <?php include 'common/footer-scripts.php';?>

    </body>
</html>
