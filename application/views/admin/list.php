<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <?php include 'common/head.php';?>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Navigation -->
        <?php include 'common/nav.php';?>

        <!-- Page Content -->
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <h1><?= $title; ?></h1>
                    <?php
                        if ($section == 'empresas')
                        {
                    ?>
                            <p class="lead">
                                Agergar empresas <a href="<?= base_url();?>index.php/empresas/create" class="btn btn-success">+</a>
                            </p>
                            <p>
                                Empresas disponibles en la base de datos:
                            </p>
                            <table id="empresas-table" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($empresas as $empresa): ?>
                                    <tr>
                                        <td>
                                            <?php echo $empresa['id']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empresa['nombre']; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('empresas/'.$empresa['id']); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                            <a href="<?php echo site_url('empresas/update_info/'.$empresa['id']); ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            <a href="<?php echo site_url('empresas/delete/'.$empresa['id']); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                    <?php
                        } elseif ($section == 'empleados') {
                    ?>
                            <p class="lead">
                                Agergar empleados <a href="<?= base_url();?>index.php/empleados/create" class="btn btn-success">+</a>
                            </p>
                            <p>
                                Empleados disponibles en la base de datos:
                            </p>
                            <table id="empresas-table" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Edad</th>
                                        <th>Empresa</th>
                                        <th>Profesional</th>
                                        <th>Skills</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Edad</th>
                                        <th>Empresa</th>
                                        <th>Profesional</th>
                                        <th>Skills</th>
                                        <th>Acciones</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php foreach ($empleados as $empleado): ?>
                                    <tr>
                                        <td>
                                            <?php echo $empleado['id']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empleado['nombre']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empleado['apellido']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empleado['edad']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empleado['empresa_id']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empleado['tipo']; ?>
                                        </td>
                                        <td>
                                            <?php echo $empleado['skills']; ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('empleados/'.$empleado['id']); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                                            <a href="<?php echo site_url('empleados/update_info/'.$empleado['id']); ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                            <a href="<?php echo site_url('empleados/delete/'.$empleado['id']); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                    <?
                        }
                    ?>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
        <div class="container-fluid">
            <hr>

            <?php include 'common/footer.php';?>
        </div>

        <?php include 'common/footer-scripts.php';?>

    </body>
</html>
