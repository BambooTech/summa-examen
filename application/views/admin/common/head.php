<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?= $title ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?= base_url(); ?>assets_fe/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets_fe/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/u/dt/dt-1.10.12/datatables.min.css"/>
    <link rel="stylesheet" href="<?= base_url(); ?>assets_fe/css/main.css">

    <script src="<?= base_url(); ?>assets_fe/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
    </style>
</head>
