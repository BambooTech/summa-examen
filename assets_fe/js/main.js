$(document).ready(function() {
    $('#developer').hide();

    $('#designer-opt').click(function() {
        if($('#designer-opt').is(':checked')) {
            $('#developer').hide();
            $('#designer').show();
        }
    });

    $('#dev-opt').click(function() {
        if($('#dev-opt').is(':checked')) {
            $('#developer').show();
            $('#designer').hide();
        }
    });

    $('#empresas-table').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );
